# File structure

```
/             - root directory of the project
.git/         - git directory
.genarbaro/   - directory with not committed data
    *external-repositories/ - directory with git external repositories

people/       - directory with personal data
relations/    - directory with relations between people
genarbaro.yml - main configuration file
```

genarbaro.yml
```
authors:
  - name
external-repositories:
  - repositoryName
    url: https://.../xyz.git
```
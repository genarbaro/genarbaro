import * as commander from "commander";
import { GenarbaroRegistry } from "./infrastructure/genarbaro.registry";

type DescriptionOpts = {
  value?: string;
};

commander
  .command("description")
  .description("repository description")
  .option("-v, --value <value>")
  .action(async (opts: DescriptionOpts) => {
    const project = await GenarbaroRegistry.create();

    if (project.manifest === undefined) {
      throw new Error("Cannot load the manifest");
    }

    // Print description
    if (opts.value === undefined) {
      project.manifest.print();
      return;
    }

    project.manifest.description = opts.value;
    await project.getRepository().saveEntity(project.manifest);
  });

/* tslint:disable-next-line */
commander.parseAsync(process.argv);

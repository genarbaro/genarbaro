#!/bin/env node
import { Command } from "commander";
import { init } from "./init";

const program = new Command();

async function main() {
  program.version("0.0.1");

  // Init command
  program.command("init").description("initialize repository").action(init);

  program.command("person", "base person operations", {
    executableFile: "person",
  });

  program.command("relation", "base relation operations", {
    executableFile: "relation",
  });

  program.command("configure", "repository configuration", {
    executableFile: "configure",
  });

  await program.parseAsync(process.argv);
}

/* tslint:disable-next-line */
main();

import { readdirSync } from "fs";
import * as path from "path";
import { Repository } from "../libs/gitisto/repository";
import * as fs from "fs";
import { Cluster } from "../libs/gitisto/cluster";
import { ManifestEntity } from "./manifest.entity";

/*
GenarbaroRegistry is responsible for .genarbaro directory and creation of repositories
*/
export class GenarbaroRegistry {
  static DIRECTORY = ".genarbaro";

  public readonly directory: string;

  private cluster: Cluster = new Cluster();
  private repository: Repository | undefined;
  private _manifest: ManifestEntity | undefined;

  private constructor() {
    const dirResult = GenarbaroRegistry.findRootDir();
    if (!dirResult) {
      throw new Error("Genarbaro cannot be found");
    }
    this.directory = dirResult;
  }

  public static async create(): Promise<GenarbaroRegistry> {
    const registry = new GenarbaroRegistry();
    await registry.load();
    // TODO: Load external registry ex. someone use other people tree
    // await registry.loadExternals();
    return registry;
  }

  public static async initialize(): Promise<void> {
    fs.mkdirSync(GenarbaroRegistry.DIRECTORY);
  }

  private static getDirectories(source: string): string[] {
    return readdirSync(source, { withFileTypes: true })
      .filter((dirent) => dirent.isDirectory())
      .map((dirent) => dirent.name);
  }

  public static findRootDir(): string | undefined {
    let dir = process.cwd();

    while (dir) {
      const found =
        this.getDirectories(dir).filter(
          (val) => val === GenarbaroRegistry.DIRECTORY
        ).length > 0;
      if (found) {
        return dir;
      }
      const newDir = path.dirname(dir);
      if (newDir === dir) {
        break;
      }
      dir = newDir;
    }
    return undefined;
  }

  private async load() {
    // Load main repository
    const repo = await Repository.create(this.directory);
    this.repository = repo;
    // Add repository to cluster
    this.cluster.addRepository(repo);

    this._manifest = await this.repository.loadEntity(ManifestEntity);
    /*const content = await readFile(
      this.getPath(entityClass.prototype.directory, entityId),
      "utf8"
    );
    const parsedFile = YAML.parse(content);*/
    // TODO: load main yml file
  }

  get manifest(): ManifestEntity | undefined {
    return this._manifest;
  }

  public getRepository(): Repository {
    if (this.repository === undefined) {
      throw new Error("Repository not found");
    }
    return this.repository;
  }
}

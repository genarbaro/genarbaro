import { IsOptional, IsString } from "class-validator";
import { Entity, Property } from "../libs/gitisto/decorators";
import { BaseEntity } from "../libs/gitisto/base-entity";

@Entity(".genarbaro", 1, "manifest.yaml")
export class ManifestEntity extends BaseEntity<ManifestEntity> {
  @IsString()
  @IsOptional()
  @Property()
  description!: string;
}

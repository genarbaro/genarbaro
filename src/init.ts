import { GenarbaroRegistry } from "./infrastructure/genarbaro.registry";

export async function init() {
  // Check if repository already exists (.genarbaro directory)
  const rootDir = GenarbaroRegistry.findRootDir();
  if (rootDir) {
    console.log("Genarbaro is already initialized");
  } else {
    await GenarbaroRegistry.initialize();
    console.log("Initialization has been finished");
  }
}

import { EntityClass, Repository } from "./repository";
import { Version, VersionState } from "./version-id";
import * as uuid from "uuid";
import * as YAML from "yaml";
import { validateOrReject } from "class-validator";
import { EntityClassType } from "./decorators";

export type PrimitivePropertyNames<T> = {
  /* tslint:disable-next-line */
  [K in keyof T]: T[K] extends Function ? never : K;
}[keyof T];

export type ExternalPropertyNames<T> = PrimitivePropertyNames<
  Omit<T, PrimitivePropertyNames<BaseEntity<any>>>
> &
  string;

export type EntityMetadata = {
  entityName: string;
  entityFilename?: string;
  version: number;
};

type PropertyParams = {
  entity: {
    path: string;
    type: EntityClassType<BaseEntity<any>>[];
  };
};

export abstract class BaseEntity<T extends object> {
  protected metadata: EntityMetadata;
  public readonly id: string;

  protected _loaded: boolean;

  get loaded(): boolean {
    return this._loaded;
  }

  constructor(
    public readonly repository: Repository,
    public readonly version: Version,
    id?: string
  ) {
    const entityVersion = Object.getPrototypeOf(this).version;
    const entityFilename = Object.getPrototypeOf(this).filename;
    const entityName = this.constructor.name;

    if (id === undefined) {
      if (entityFilename !== undefined) {
        this.id = "";
        // this._loaded = true;
        this._loaded = false;
      } else {
        this.id = uuid.v4();
        this._loaded = true;
      }
    } else {
      this.id = id;
      this._loaded = false;
    }

    this.metadata = {
      entityName,
      entityFilename,
      version: entityVersion,
    };
  }

  public async load() {
    const content = await this.repository.loadEntityContent(this);

    // FIXME: it is too early, but it is needed to set some properties during loading
    this._loaded = true;
    const parsedFile = YAML.parse(content);
    if (parsedFile?.entityName !== this.metadata.entityName) {
      throw new Error("Wrong data passed to entity.");
    }

    if (parsedFile?.version === undefined) {
      throw new Error("Missing version in metadata.");
    }

    if (parsedFile.version > this.metadata.version) {
      throw new Error("Version not supported yet. Try to update application.");
    }
    // FIXME: What if version is older? Does this check make sense?
    this.loadExtras(parsedFile.extras);
    this.loadPropsData(parsedFile ?? {});
  }

  private loadExtras(input: any) {
    // Load extras
    const extras = input ?? {};
    this.extras.forEach((extra) => {
      Object.assign(this, { [extra]: extras[extra] });
    });
  }

  protected storeExtras(): { [key: string]: any } {
    const data = this.extras.reduce(
      (prev: { [key: string]: any }, curr: string) => {
        prev[curr] = (this as any)[curr];
        return prev;
      },
      {}
    );
    return data;
  }

  protected deserializeField(
    obj: any,
    field: string | number,
    objpath: string[],
    entities: EntityClass<any>[]
  ): any {
    if (objpath.length < 1) {
      // Get id and name
      const [name, id] = obj[field].split(":");
      // find correct entity
      const entityClasses = entities.filter((entity) => entity.name === name);
      if (entityClasses.length !== 1) {
        console.log(entityClasses);
        throw new Error("There is a problem with entity class detection");
      }

      const entityClass = entityClasses[0];

      obj[field] = new entityClass(
        this.repository,
        new Version(VersionState.WORKSPACE, ""),
        id
      );
      return;
    }

    switch (objpath[0]) {
      // If it is an array
      case "?":
        const array = obj[field];
        for (let i = 0; i < array.length; i++) {
          this.deserializeField(obj[field], i, objpath.slice(1), entities);
        }
        break;
      case "!":
        this.deserializeField(obj, field, objpath.slice(1), entities);
        break;
      default:
        this.deserializeField(
          obj[field],
          objpath[0],
          objpath.slice(1),
          entities
        );
    }
  }

  /**
   * YAML to object fields assignment
   *
   * @returns yaml string
   */
  protected loadPropsData(parsedFile: { [key: string]: any }): void {
    const data = parsedFile.data ?? {};
    const props = this.fieldsProps;
    this.fields.forEach((field) => {
      if (data[field] === undefined || data[field] === null) {
        return;
      }

      // If easy to detect
      const t = Reflect.getMetadata("design:type", this, field);
      if (t.prototype instanceof BaseEntity) {
        const [, id] = (data[field] as string).split(":");
        const entity = new t(
          this.repository,
          new Version(VersionState.WORKSPACE, ""),
          id
        );

        Object.assign(this, {
          [field]: entity,
        });
        return;
      }

      // If property defined
      const prop = props[field];
      if (prop !== undefined && prop.entity !== undefined) {
        const objpath = prop.entity.path.split("/");
        this.deserializeField(data, field, objpath, prop.entity.type);
      }
      Object.assign(this, {
        [field]: data[field],
      });
    });
  }

  /**
   * Method rebuilds object's fields but it is taking BaseEntity into account
   */
  protected serializeField(field: any): any {
    // Skip if it's a function - it should not happen
    if (typeof field === "function") {
      return undefined;
    }
    // return if it's a primitive
    if (typeof field !== "object" || field === null) {
      return field;
    }

    if (field instanceof BaseEntity) {
      return field.metadata.entityName + ":" + field.id;
    }
    // if field is an object
    if (Array.isArray(field)) {
      const array = [];
      for (const prop in field) {
        if (field.hasOwnProperty(prop)) {
          array.push(this.serializeField(field[prop]));
        }
      }
      return array;
    }
    const obj: any = {};
    for (const prop in field) {
      if (field.hasOwnProperty(prop)) {
        obj[prop] = this.serializeField(field[prop]);
      }
    }
  }

  /**
   * Returns entity data. Used for yaml serialization
   */
  protected storePropsData(): { [key: string]: any } {
    const entityData: any = this;
    const data = this.fields.reduce(
      (prev: { [key: string]: any }, curr: string) => {
        prev[curr] = this.serializeField(entityData[curr]);
        return prev;
      },
      {}
    );
    return { data };
  }

  public setFromSchema<K extends ExternalPropertyNames<T>>(
    param: K,
    value: T[K]
  ) {
    this.set(param, value);
  }

  public set(param: string, value: any, validation: boolean = true) {
    if (
      validation &&
      this.fields.find((field) => field === param) === undefined
    ) {
      throw new Error("Wrong parameter");
    } else if (param === "metadata") {
      throw new Error("Metadata cannot be set");
    }
    Object.assign(this, { [param]: value });
  }

  /**
   * Method responsible for preparing YAML data
   *
   * @returns yaml string
   */
  public cook(): string {
    // FIXME:
    validateOrReject(this).catch((errors) => {
      console.log("Promise rejected (validation failed). Errors: ", errors);
    });

    const data = this.storePropsData();
    const extras = this.storeExtras();

    const obj = {
      entityName: this.constructor.name,
      version: Object.getPrototypeOf(this).version,
      ...(extras ? { extras } : {}),
      ...data,
    };
    return YAML.stringify(obj);
  }

  /**
   * Returns list of the all fields
   */
  get fieldsProps(): { [key: string]: PropertyParams } {
    let props = {};
    let target = Object.getPrototypeOf(this);
    while (target !== Object.prototype) {
      const childFields = Reflect.getOwnMetadata("fieldslist", target) || {};
      props = { ...props, ...childFields };
      target = Object.getPrototypeOf(target);
    }
    return props;
  }

  /**
   * Returns list of the all fields
   */
  get fields(): string[] {
    const list = Object.keys(this.fieldsProps);
    return list;
  }

  /**
   * Returns list of the extra fields which can be used by some bese classes
   */
  protected get extras(): string[] {
    const extras = [];
    let target = Object.getPrototypeOf(this);
    while (target !== Object.prototype) {
      const childFields = Reflect.getOwnMetadata("extras", target) || [];
      extras.push(...childFields);
      target = Object.getPrototypeOf(target);
    }
    return extras;
  }

  /**
   * Printing to console method
   */
  public print() {
    // If no id provided, skip
    if (this.id !== "") {
      console.log(`Id: ${this.id}`);
    }
    console.log(YAML.stringify(this.storePropsData()));
  }

  public toJSON(): any {
    return {
      id: this.id,
      metadata: this.metadata,
      ...this.storePropsData(),
    };
  }
}

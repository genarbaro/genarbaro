import { Repository } from "./repository";

export class Cluster {
  protected repositories: Repository[] = [];

  public constructor() {
    // TODO:
  }

  public addRepository(repo: Repository): void {
    this.repositories.push(repo);
  }

  public getRepositories(): Repository[] {
    return this.repositories;
  }
}

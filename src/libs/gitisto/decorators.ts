import "reflect-metadata";
import { BaseEntity } from "./base-entity";
import { EntityPath } from "./entity-path";

/* tslint:disable:only-arrow-functions ban-types */
export const Entity = (
  directory: string,
  version: number,
  filename: string | undefined = undefined
) => {
  return function (constructorFunction: Function) {
    constructorFunction.prototype.directory = directory;
    constructorFunction.prototype.version = version;
    constructorFunction.prototype.filename = filename;
  };
};

export type EntityClassType<T extends BaseEntity<any>> = new (
  ...args: any[]
) => T;

export type PropertyParams<T extends object, W extends BaseEntity<any>> = {
  entity: {
    path: EntityPath<T, W>;
    type: EntityClassType<BaseEntity<any>>[];
  };
};

export function Property<
  T extends object,
  W extends BaseEntity<any> = BaseEntity<any>
>(params?: PropertyParams<T, W>): PropertyDecorator {
  return (target, key: string | symbol) => {
    const fields = Reflect.getOwnMetadata("fieldslist", target) || {};
    if (!fields.hasOwnProperty(key)) {
      fields[key] = params;
    }
    Reflect.defineMetadata("fieldslist", fields, target);

    const descriptor = {
      get(): any {
        if (!(this as any).loaded) {
          throw new Error("Cannot read not loaded property");
        }
        return (this as any)[`_${key.toString()}`];
      },
      set(val: any) {
        if (!(this as any).loaded) {
          throw new Error("Cannot edit not loaded property");
        }
        (this as any)[`_${key.toString()}`] = val;
      },
      enumerable: true,
      configurable: true,
    };
    Object.defineProperty(target, key, descriptor);
  };
}

export function Extra(): PropertyDecorator {
  return (target, key) => {
    const extras = Reflect.getOwnMetadata("extras", target) || [];
    if (!extras.includes(key)) {
      extras.push(key);
    }
    Reflect.defineMetadata("extras", extras, target);
  };
}

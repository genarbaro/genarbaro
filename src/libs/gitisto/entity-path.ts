import { BaseEntity } from "./base-entity";

/* tslint:disable:only-arrow-functions ban-types array-type*/

// Define simple key
type Key = string | number | symbol;

// Join 2 strings
type Join<L extends Key | undefined, R extends Key | undefined> = L extends
  | string
  | number
  ? R extends string | number
    ? `${L}/${R}`
    : L
  : R extends string | number
  ? R
  : undefined;

// Type union
type Union<
  L extends unknown | undefined,
  R extends unknown | undefined
> = L extends undefined
  ? R extends undefined
    ? undefined
    : R
  : R extends undefined
  ? L
  : L | R;

type ValidObject<T, I> = T extends object
  ? T extends I
    ? false & 1
    : T
  : false & 1;

type Unpacked<T> = T extends (infer U)[]
  ? U
  : T extends (...args: any[]) => infer P
  ? P
  : T extends Promise<infer R>
  ? R
  : T;

export type TypePathN<
  T extends object,
  W,
  I,
  Prev extends Key | undefined = undefined,
  Path extends Key | undefined = undefined,
  PrevTypes extends object = T
> = T extends Array<any>
  ? TypePathN<
      Unpacked<T>,
      W,
      I,
      Union<Prev, Path>,
      Join<Path, `?`>,
      PrevTypes | T
    >
  : T extends W
  ? Path
  : {
      [K in keyof T]: T[K] extends PrevTypes | T // T[K] is a type alredy checked?
        ? Required<T>[K] extends W
          ? Join<Path, K>
          : never
        : // T[K] is an object?.
        Required<T>[K] extends ValidObject<Required<T>[K], I>
        ? Required<T>[K] extends Array<any>
          ? // Array
            TypePathN<
              Unpacked<Required<T>[K]>,
              W,
              I,
              Union<Prev, Path>,
              Join<Path, `${K & string}/?`>,
              PrevTypes | T
            >
          : // Object
            TypePathN<
              Required<T>[K],
              W,
              I,
              Union<Prev, Path>,
              Join<Path, K>,
              PrevTypes | T
            >
        : // Return
        Required<T>[K] extends W
        ? Join<Path, K>
        : never;
    }[keyof T];

type IfAny<T, Y, N> = 0 extends 1 & T ? Y : N;

export type TypePath<T extends object, W, C> =
  // If it is array of needed type
  T extends W[] ? "?" : T extends W ? "!" : TypePathN<T, W, C>;

export type EntityPath<T extends object, W extends BaseEntity<any>> = TypePath<
  T,
  W,
  BaseEntity<any>
>;

///////////////////////////////
/*

export type PropertyParams<T extends object, W extends BaseEntity<any>> = {
  entity: {
    path: EntityPath<T, W>;
  };
};

export function Property<
  T extends object,
  W extends BaseEntity<any> = BaseEntity<any>
>(params?: PropertyParams<T, W>): PropertyDecorator {
  return (target, key: string | symbol) => {};
}

class NestedEntity extends BaseEntity<NestedEntity> {
  @Property()
  value!: number;
}

interface XYZ {
  a: NestedEntity;
  d: {
    w: NestedEntity[];
  };
}

class MainEntity extends BaseEntity<MainEntity> {
  @Property()
  somevalue!: string;

  @Property<XYZ>({
    entity: { path: "a" },
  })
  member!: XYZ;

  @Property<XYZ[]>({
    entity: { path: "?/d/w/?" },
  })
  members!: XYZ[];
}

const r: EntityPath<XYZ, BaseEntity<any>> = "a";

/*
interface Member {
  w: BaseEntity<any>;
}

@Entity("relation", 1)
export class RelationEntity extends BaseEntity<RelationEntity> {
  members!: Member[];
}
const p: EntityPath<RelationEntity[], RelationEntity> = "dsafs*";
const r: EntityPath<RelationEntity, BaseEntity<any>> = "!";
*/
/*
class P1 {
  public p2?: P2;
}

class P2 {
  public field: number = 0;
  public w: boolean = true;
  public n?: number[];
  public p?: { xyz: number }[];
  public z?: { xyz: number };
  public p1?: P1;
}

type VP = TypePath<P1, any, P2>;
const vps: VP = "p2";

type ZS = TypePath<P1, any, P1>;
const zs1: ZS[] = ["p2/p1"];

type A1 = TypePath<P1, P1, P1>;
const a1: A1 = "!";

type A2 = TypePath<P1[], P1, P1>;
const a2: A2 = "*";
*/

import { BaseEntity } from "./base-entity";
import { Version, VersionState } from "./version-id";
import * as fs from "fs";
import * as util from "util";
import * as path from "path";
import { execFileSync } from "child_process";
import * as rimraf from "rimraf";

const readFile = util.promisify(fs.readFile);
const exists = util.promisify(fs.exists);
const writeFile = util.promisify(fs.writeFile);

function flatten(arr: any) {
  return arr.reduce((flat: any, toFlatten: any) => {
    return flat.concat(
      Array.isArray(toFlatten) ? flatten(toFlatten) : toFlatten
    );
  }, []);
}

function runGitCommand(cwd: string | undefined, args: string[]) {
  const execOptions = { cwd };
  return execFileSync("git", args, execOptions).toString();
}

export type EntityClass<T> = new (
  repository: Repository,
  version: Version,
  content?: string,
  id?: string
) => T;

/**
 * Responsible for git operations on git repository. It knows how entity works and what is the index
 */
export class Repository {
  private constructor(public readonly directory: string) {}

  /**
   * It loads repository or if doesn't exist than create a new one.
   *
   * @param directory - full path to the git repository
   */
  public static async create(directory: string): Promise<Repository> {
    // If doesn't exists create
    if (!(await exists(`${directory}/.git`))) {
      try {
        runGitCommand(undefined, [
          "-c",
          "init.defaultBranch=master",
          "init",
          directory,
        ]);
        runGitCommand(directory, [
          "commit",
          "--allow-empty",
          "-m",
          "Initial commit",
        ]);
      } catch (error) {
        return Promise.reject(error);
      }
    }
    return new Repository(directory);
  }

  /**
   * Removes `.git` directory. Use carefully!!!
   */
  public async nuke() {
    rimraf.sync(`${this.directory}/.git`);
  }

  /**
   * Pulls all data from remote repository
   */
  public async pull(): Promise<void> {
    // TODO:
    // Rebuild indexes
    await this.buildIndex();
  }

  /**
   * Pushes all data to remote repository
   */
  public async push(): Promise<void> {
    // TODO:
  }

  /**
   * This method is called when index have to be rebuilt
   */
  protected async buildIndex(): Promise<void> {
    // TODO:
  }

  /**
   * Commits changes to local repository
   */
  public async commit(commitMsg: string): Promise<void> {
    // TODO:
  }

  /**
   * Removes all changes in workspace
   */
  public async rollback(): Promise<void> {
    // TODO:
  }

  /**
   * Creates empty version of entity.
   *
   * @param {EntityClass<T>} entityClass - Class which should be created.
   */
  public async createEntity<T extends BaseEntity<any>>(
    entityClass: EntityClass<T>
  ) {
    return new entityClass(this, Version.createNewVersion());
  }

  /**
   * Load entity from the workspace (drive) or from committed version
   *
   * @param {EntityClass<T>} entityClass - Class which should be created from file
   * @param {string} entityId - Entity id used to find a place with file
   * @param {Version} versionId? - Version of the entity. If is not provided then it is loaded from workspace.
   */
  public async loadEntity<T extends BaseEntity<any>>(
    entityClass: EntityClass<T>,
    entityId?: string,
    versionId?: Version
  ): Promise<T> {
    const uniqueEntity = entityClass.prototype.filename !== undefined;

    // If version id doesn't exist, workspace version is used
    let content;
    if (versionId === undefined) {
      let pathVal;
      if (uniqueEntity) {
        pathVal = this.getUniquePath(
          entityClass.prototype.directory,
          entityClass.prototype.filename
        );
        // FIXME: It should be in a different place
        // If missing create empty
        if (!(await exists(pathVal))) {
          return this.createEntity<T>(entityClass);
        }
      } else {
        if (entityId === undefined) {
          throw new Error("No Id provided");
        }
        pathVal = this.getIdPath(entityClass.prototype.directory, entityId);
      }

      try {
        content = await readFile(pathVal, "utf8");
      } catch (error) {
        return Promise.reject<T>("Missing entity");
      }
      // TODO: If file is from workspace but is committed it contains version.
    } else {
      // TODO: Load content from path
      content = "";
      // TODO: Check tree id
    }

    const p = new entityClass(
      this,
      versionId ?? new Version(VersionState.WORKSPACE, ""), // TODO: more states
      entityId || "" // Empty if it is unique entity
    );
    await p.load();
    return p;
  }

  public async loadEntityContent<T extends object>(
    entity: BaseEntity<T>
  ): Promise<string> {
    // entity.version
    return await readFile(this.getEntityPath(entity), "utf8");
  }

  /*
   * Stage entity to the index
   */
  public async stageEntity<T extends BaseEntity<any>>(entity: T) {
    // TODO:
  }

  /*
   * Unstage entity to the index
   */
  public async unstageEntity<T extends BaseEntity<any>>(entity: T) {
    // TODO:
  }

  /*
   * Save entity to the workspace
   */
  public async saveEntity<T extends BaseEntity<any>>(entity: T) {
    switch (entity.version.state) {
      // If entity exists only in memory it means that is a new one. No data should be overridden.
      case VersionState.MEMORY:
        if (await exists(this.getEntityPath(entity))) {
          throw new Error("File already exists");
        }
        await this.writeEntity(entity);
        // TODO: Change state of the entity version
        break;
      case VersionState.WORKSPACE:
        if (!(await exists(this.getEntityPath(entity)))) {
          throw new Error("File not exists");
        }
        await this.writeEntity(entity);
        // TODO: Change state of the entity version
        break;
      default:
        throw new Error("Wrong state of the entity");
    }
  }

  private async writeEntity(entity: BaseEntity<any>) {
    const filePath = this.getEntityPath(entity);

    // Create missing directories
    await fs.promises.mkdir(path.dirname(filePath), { recursive: true });

    await writeFile(filePath, await entity.cook());
    entity.version.saved();
  }

  public async list<T>(entityClass: EntityClass<T>): Promise<string[]> {
    const list = await this.getFileNames(entityClass.prototype.directory);
    // Convert to ids
    return list.map((pathFile) => pathFile.replace(/\//g, "-"));
  }

  private async getFileNames(directory: string): Promise<string[]> {
    const files = await fs.promises.readdir(directory);

    const result: (string | string[])[] = await Promise.all(
      files.map(async (file) => {
        const p = directory + "/" + file;
        const stat = await fs.promises.stat(p);
        if (stat.isDirectory()) {
          return (await this.getFileNames(directory + "/" + file)).map(
            (name) => `${path.basename(p)}/${name}`
          );
        } else {
          return `${file}`;
        }
      })
    );

    return flatten(result);
  }

  protected getEntityPath(entity: BaseEntity<any>): string {
    const prefix = Object.getPrototypeOf(entity).directory;
    if (!prefix) {
      throw new Error("It is not entity");
    }
    // get filename in case of unique entity
    const filename = Object.getPrototypeOf(entity).filename;

    if (filename === undefined) {
      return this.getIdPath(prefix, entity.id);
    }
    return this.getUniquePath(prefix, filename);
  }

  protected getUniquePath(prefix: string, filename: string): string {
    return `${this.directory}/${prefix}/${filename}`;
  }

  protected getIdPath(prefix: string, id: string): string {
    const idPath = id.replace(/-/g, "/");
    return `${this.directory}/${prefix}/${idPath}`;
  }
}

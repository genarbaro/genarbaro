import * as fs from "fs";
import * as path from "path";
import rimraf = require("rimraf");
import { Repository } from "../repository";
import { Cluster } from "../cluster";

/* tslint:disable:max-classes-per-file */

describe("Cluster", () => {
  const pathFixture = `${__dirname}/fixtures/cluster`;

  beforeAll(async () => {
    // create fixture dir
    await fs.promises.mkdir(path.dirname(pathFixture), { recursive: true });
  });

  afterAll(async () => {
    // remove dir
    rimraf.sync(`${pathFixture}/cluster`);
  });

  describe("Repository", () => {
    afterAll(async () => {
      // TODO:
    });

    it("add repository", async () => {
      // Given
      const repository = await Repository.create(pathFixture + "/repo1");

      // When
      const cluster = new Cluster();
      cluster.addRepository(repository);

      // Then
      expect(cluster).toEqual(
        expect.objectContaining({ repositories: [expect.any(Repository)] })
      );
    });
  });
});

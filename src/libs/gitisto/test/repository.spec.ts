jest.mock("uuid", () => ({
  v4: jest.fn(),
}));

import * as fs from "fs";
import { Repository } from "../repository";
import * as YAML from "yaml";
import { BaseEntity } from "../base-entity";
import { Entity, Property } from "../decorators";
import { Version, VersionState } from "../version-id";
import * as path from "path";
import rimraf = require("rimraf");

/* tslint:disable:no-var-requires */
const mockUuid = require("uuid") as { v4: jest.Mock<string, []> };

/* tslint:disable:max-classes-per-file */

describe("Repository", () => {
  const pathFixture = `${__dirname}/fixtures`;

  beforeAll(async () => {
    // create fixture dir
    await fs.promises.mkdir(path.dirname(pathFixture), { recursive: true });
  });

  afterAll(async () => {
    // remove entity dir
    rimraf.sync(`${pathFixture}/example-entity`);
  });

  describe("Life cycle", () => {
    it("should create repository", async () => {
      // When
      await Repository.create(pathFixture);

      // Then
      expect(fs.existsSync(`${pathFixture}/.git`)).toBeTruthy();
    });

    it("should remove repository", async () => {
      // Given
      const repo = await Repository.create(pathFixture);

      // When
      await repo.nuke();

      // Then
      expect(fs.existsSync(`${pathFixture}/.git`)).toBeFalsy();
    });
  });

  async function writeYamlFile(filePath: string, data: any): Promise<void> {
    // Create missing directories
    await fs.promises.mkdir(path.dirname(filePath), { recursive: true });
    return fs.promises.writeFile(filePath, YAML.stringify(data));
  }

  async function readYamlFile(filePath: string): Promise<any> {
    return YAML.parse(await fs.promises.readFile(filePath, "utf-8"));
  }

  describe("Methods", () => {
    let repository: Repository;

    beforeAll(async () => {
      repository = await Repository.create(pathFixture);
    });

    afterAll(async () => {
      await repository.nuke();
    });

    describe("Create Entity", () => {
      it("should create entity", async () => {
        // Given
        @Entity("example-entity", 1)
        class ExampleEntity extends BaseEntity<ExampleEntity> {}
        mockUuid.v4.mockImplementation(() => "example-id");

        // When
        const entity = await repository.createEntity(ExampleEntity);

        // Then
        expect(entity).toEqual(
          expect.objectContaining({
            id: expect.any(String),
            repository: expect.any(Repository),
            version: { versionState: VersionState.MEMORY }, // Entity is only in memory
          })
        );
      });
    });

    describe("Load Entity", () => {
      it("should load entity from workspace", async () => {
        // Given
        @Entity("example-entity", 1)
        class ExampleEntity extends BaseEntity<ExampleEntity> {
          @Property()
          name!: string;

          @Property()
          data!: string;
        }

        await writeYamlFile(`${pathFixture}/example-entity/some/example/id`, {
          entityName: "ExampleEntity",
          version: 1,
          data: {
            name: "ExampleName",
            data: "ExampleData",
          },
        });

        // When
        const entity = await repository.loadEntity(
          ExampleEntity,
          "some-example-id"
        );

        // Then
        expect(entity).toEqual(
          expect.objectContaining({ data: "ExampleData", name: "ExampleName" })
        );
      });

      it("error: missing entity", async () => {
        @Entity("example-entity", 1)
        class ExampleEntity extends BaseEntity<ExampleEntity> {}
        const entity = repository.loadEntity(ExampleEntity, "missing-id");
        await expect(entity).rejects.toEqual("Missing entity");
      });

      it("should load entity from ", async () => {
        // TODO
      });
    });

    describe("Save Entity", () => {
      it("should save entity to workspace", async () => {
        // Given
        @Entity("example-entity", 1)
        class ExampleEntity extends BaseEntity<ExampleEntity> {
          @Property()
          value!: number;
        }
        mockUuid.v4.mockImplementation(() => "example-id");

        const entity = await repository.createEntity(ExampleEntity);
        entity.set("value", 2);

        // When
        await repository.saveEntity(entity);

        // Then
        // Check if file exists with a good content
        const data = await readYamlFile(
          `${pathFixture}/example-entity/example/id`
        );
        expect(data).toEqual(expect.objectContaining({ data: { value: 2 } }));
        // Check state
        expect(entity.version).toEqual({
          versionState: VersionState.WORKSPACE,
        });
      });
    });

    describe("Nested entity", () => {
      @Entity("nested-entity", 1)
      class NestedEntity extends BaseEntity<NestedEntity> {
        @Property()
        value!: number;
      }

      describe("Directy", () => {
        @Entity("main-entity", 1)
        class MainEntity extends BaseEntity<MainEntity> {
          @Property()
          somevalue!: string;

          @Property()
          nested!: NestedEntity;
        }

        beforeAll(async () => {
          mockUuid.v4.mockImplementation(() => "entity");
        });

        afterAll(async () => {
          // remove entity dir
          rimraf.sync(`${pathFixture}/main-entity`);
          rimraf.sync(`${pathFixture}/nested-entity`);
        });

        it("Attach nested entity and load", async () => {
          // Given
          const e1 = await repository.createEntity(MainEntity);
          e1.setFromSchema("somevalue", "example");
          await repository.saveEntity(e1);
          const e2 = await repository.createEntity(NestedEntity);
          e2.setFromSchema("value", 2);
          await repository.saveEntity(e2);

          const mainEntity = await repository.loadEntity(MainEntity, "entity");
          const nestedEntity = await repository.loadEntity(
            NestedEntity,
            "entity"
          );

          // When
          mainEntity.setFromSchema("nested", nestedEntity);
          await repository.saveEntity(mainEntity);

          // Then

          // Check if file exists with a good content
          const data = await readYamlFile(`${pathFixture}/main-entity/entity`);
          expect(data).toEqual(
            expect.objectContaining({
              data: { nested: "NestedEntity:entity", somevalue: "example" },
            })
          );

          // Load nested
          const checkEntity = await repository.loadEntity(MainEntity, "entity");
          expect(checkEntity.nested.loaded).toEqual(false);
          await checkEntity.nested.load();
          expect(checkEntity.nested.value).toEqual(2);
        });
      });

      describe("Array of entities", () => {
        @Entity("main-entity", 1)
        class MainEntity extends BaseEntity<MainEntity> {
          @Property()
          somevalue!: string;

          @Property<NestedEntity[]>({
            entity: { path: "?", type: [NestedEntity] },
          })
          members!: NestedEntity[];
        }

        afterAll(async () => {
          // remove entity dir
          rimraf.sync(`${pathFixture}/main-entity`);
          rimraf.sync(`${pathFixture}/nested-entity`);
        });

        it("Attach nested entity and load", async () => {
          // Given
          mockUuid.v4.mockImplementationOnce(() => "entity");
          const e1 = await repository.createEntity(MainEntity);
          e1.setFromSchema("somevalue", "example");
          await repository.saveEntity(e1);
          mockUuid.v4.mockImplementationOnce(() => "nested1");
          const e2 = await repository.createEntity(NestedEntity);
          e2.setFromSchema("value", 3);
          await repository.saveEntity(e2);
          mockUuid.v4.mockImplementationOnce(() => "nested2");
          const e3 = await repository.createEntity(NestedEntity);
          e2.setFromSchema("value", 3);
          await repository.saveEntity(e3);

          const mainEntity = await repository.loadEntity(MainEntity, "entity");
          const nestedEntity1 = await repository.loadEntity(
            NestedEntity,
            "nested1"
          );
          const nestedEntity2 = await repository.loadEntity(
            NestedEntity,
            "nested2"
          );

          // When
          mainEntity.setFromSchema("members", [nestedEntity1, nestedEntity2]);
          await repository.saveEntity(mainEntity);

          // Then

          // Check if file exists with a good content
          const data = await readYamlFile(`${pathFixture}/main-entity/entity`);
          expect(data).toEqual(
            expect.objectContaining({
              data: {
                members: ["NestedEntity:nested1", "NestedEntity:nested2"],
                somevalue: "example",
              },
            })
          );

          // Load nested
          const checkEntity = await repository.loadEntity(MainEntity, "entity");
          expect(checkEntity.members.length).toEqual(2);
          await checkEntity.members[0].load();
          expect(checkEntity.members[0].value).toEqual(3);
          // Not loaded
          const t = () => {
            return checkEntity.members[1].value;
          };
          expect(t).toThrow(Error);
        });
      });
    });

    describe("Entity States", () => {
      @Entity("example-entity", 1)
      class ExampleEntity extends BaseEntity<ExampleEntity> {
        @Property()
        value!: number;
      }

      beforeAll(async () => {
        mockUuid.v4.mockImplementation(() => "entity");
      });

      let entity: ExampleEntity;

      it("should keep entity in MEMORY", async () => {
        // When
        entity = await repository.createEntity(ExampleEntity);

        entity.set("value", 2);

        // Then
        // Check state
        expect(entity.version).toEqual({ versionState: VersionState.MEMORY });
      });

      it("should save entity to WORKSPACE", async () => {
        // Given
        // prev state

        // When
        await repository.saveEntity(entity);

        // Then

        // Check if file exists with a good content
        const data = await readYamlFile(`${pathFixture}/example-entity/entity`);
        expect(data).toEqual(expect.objectContaining({ data: { value: 2 } }));
        // Check state
        expect(entity.version).toEqual({
          versionState: VersionState.WORKSPACE,
        });
      });

      it("should stage entity (INDEX)", async () => {
        // Given
        // prev state

        // When
        await repository.stageEntity(entity);

        // Then
        // FIXME:
        // expect(entity.version).toEqual({ versionState: VersionState.INDEX });
      });

      it("should unstage entity", async () => {
        // Given
        // prev state

        // When
        await repository.unstageEntity(entity);

        // Then
        expect(entity.version).toEqual({
          versionState: VersionState.WORKSPACE,
        });
      });
    });
  });
});

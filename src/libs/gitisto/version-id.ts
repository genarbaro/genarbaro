export enum VersionState {
  MEMORY = 1,
  WORKSPACE = 2,
  INDEX = 3,
  COMMITTED = 4,
}

/*
Represents a git sha1
*/

export class Version {
  private versionId: string | undefined;
  private versionState: VersionState;

  constructor(state: VersionState, id?: string) {
    this.versionState = state;
  }

  get state(): VersionState {
    return this.versionState;
  }

  public changed() {
    this.versionState = VersionState.MEMORY;
  }

  public saved() {
    this.versionState = VersionState.WORKSPACE;
  }

  get id(): string | undefined {
    return this.versionId;
  }

  static createNewVersion(): Version {
    return new Version(VersionState.MEMORY, undefined);
  }

  public setVersion(id: string) {
    this.versionId = id;
  }
}

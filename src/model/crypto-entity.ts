import { isEmpty } from "validate.js";
import { BaseEntity } from "../libs/gitisto/base-entity";
import { Extra } from "../libs/gitisto/decorators";
import * as crypto from "crypto";

export type Encryption = {
  // FIXME: Currently it is used as a key
  keyName: string;
  iv: string;
};
const encryptionAlgorithm = "aes-256-cbc";

export class CryptoEntity<T extends object> extends BaseEntity<T> {
  @Extra()
  private encryption!: Encryption | undefined;

  private encryptedFields: string[] = [];

  protected override loadPropsData(parsedFile: { [key: string]: any }): void {
    if (this.encryptedFields === undefined) {
      this.encryptedFields = [];
    }

    // Skip if not encrypted data
    const encryptedData = parsedFile.encryptedData;
    if (encryptedData && !isEmpty(encryptedData)) {
      if (this.encryption === undefined) {
        throw new Error("Encryption is not set");
      }
      // FIXME:

      const decipher = crypto.createDecipheriv(
        encryptionAlgorithm,
        Buffer.from(this.encryption.keyName, "hex"),
        Buffer.from(this.encryption.iv, "hex")
      );

      this.fields.forEach((field) => {
        if (encryptedData[field] === undefined) {
          return;
        }

        const encryptedText = Buffer.from(encryptedData[field], "hex");
        const decrypted = decipher.update(encryptedText);
        const data = Buffer.concat([decrypted, decipher.final()]).toString();
        if (parsedFile.data === null || parsedFile.data === undefined) {
          parsedFile.data = {};
        }
        Object.assign(parsedFile.data, { [field]: data });
        this.encryptedFields.push(field);
      });
    }

    super.loadPropsData(parsedFile);
  }

  /**
   * Returns entity data. Used for yaml serialization
   */
  protected override storePropsData(): { [key: string]: any } {
    const { data } = super.storePropsData();
    // Skip if no encrypted data
    if (!this.encryptedFields || isEmpty(this.encryptedFields)) {
      return { data };
    }

    // FIXME: if has been changed generate new iv
    if (this.encryption === undefined) {
      // FIXME: key
      this.encryption = {
        keyName: crypto.randomBytes(32).toString("hex"),
        iv: crypto.randomBytes(16).toString("hex"),
      };
    }

    const cipher = crypto.createCipheriv(
      encryptionAlgorithm,
      Buffer.from(this.encryption.keyName, "hex"),
      Buffer.from(this.encryption.iv, "hex")
    );
    const encryptedData = this.fields.reduce(
      (prev: { [key: string]: any }, curr: string) => {
        if (this.encryptedFields.includes(curr)) {
          const encrypted = cipher.update(data[curr]);
          delete data[curr];
          prev[curr] = Buffer.concat([encrypted, cipher.final()]).toString(
            "hex"
          );
        }
        return prev;
      },
      {}
    );
    return { data, encryptedData };
  }

  public setPropertyEncription(name: string, status: boolean) {
    if (status) {
      this.encryptedFields.push(name);
    } else {
      const index = this.encryptedFields.indexOf(name);
      if (index !== -1) {
        this.encryptedFields.splice(index, 1);
      }
    }
  }

  /**
   * Printing to console method
   */
  public override print() {
    // If no id provided, skip
    if (this.id !== "") {
      console.log(`Id: ${this.id}`);
    }

    this.fields.forEach((field) => {
      console.log(`${field} : ${(this as any)[field]}`);
    }, {});
  }
}

import { IsOptional, IsString } from "class-validator";
import { Entity, Property } from "../libs/gitisto/decorators";
import { CryptoEntity } from "./crypto-entity";

@Entity("person", 1)
export class PersonEntity extends CryptoEntity<PersonEntity> {
  @IsString()
  @IsOptional()
  @Property()
  name!: string;

  public print() {
    console.log("Person");
    super.print();
  }
}

import { Entity, Property } from "../libs/gitisto/decorators";
import { BaseEntity } from "../libs/gitisto/base-entity";
import { PersonEntity } from "./person.entity";

interface Member {
  self: RelationEntity | PersonEntity;
  role: string;
}

@Entity("relation", 1)
export class RelationEntity extends BaseEntity<RelationEntity> {
  @Property<Member[]>({
    entity: { path: "?/self", type: [RelationEntity, PersonEntity] },
  })
  members!: Member[];
}

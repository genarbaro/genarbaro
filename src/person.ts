import * as commander from "commander";
import { GenarbaroRegistry } from "./infrastructure/genarbaro.registry";
import { PersonEntity } from "./model/person.entity";

commander
  .command("create")
  .description("create a new person file")
  .option("-i, --interactive") // TODO: Add support
  .action(async (opts) => {
    const project = await GenarbaroRegistry.create();

    const repository = project.getRepository();
    const entity = await repository.createEntity(PersonEntity);
    console.log(`New entity id: ${entity.id}`);
    // Save empty
    await repository.saveEntity(entity);
  });

type PersonShowOpts = {
  uuid: string;
};

commander
  .command("show")
  .description("show all person data")
  .option("-id, --uuid <uuid>")
  .action(async (opts: PersonShowOpts) => {
    const project = await GenarbaroRegistry.create();

    const repository = project.getRepository();
    const person = await repository.loadEntity(PersonEntity, opts.uuid);
    person.print();
  });

commander
  .command("list")
  .description("list all people")
  .action(async (opts) => {
    const project = await GenarbaroRegistry.create();

    const repository = project.getRepository();
    const uuids = await repository.list(PersonEntity);
    const entities = await Promise.all(
      uuids.map(async (uuid) => {
        return repository.loadEntity(PersonEntity, uuid);
      })
    );

    entities.forEach((entity) => {
      entity.print();
    });
  });

type PersonEditOpts = {
  uuid?: string;
  param?: string;
  value?: string;
  encrypt: boolean;
  decrypt: boolean;
};

commander
  .command("edit")
  .description("edit person")
  .option("-id, --uuid <uuid>")
  .option("-p, --param <param>")
  .option("-v, --value <value>")
  .option("-e, --encrypt")
  .option("-d, --decrypt")
  .action(async (opts: PersonEditOpts) => {
    const project = await GenarbaroRegistry.create();

    if (opts.uuid === undefined) {
      console.log(`Missing uuid. Check "$ genarbaro person help edit"`);
      return;
    }

    if (opts.param === undefined) {
      console.log(`Missing param. Check "$ genarbaro person help edit"`);
      return;
    }

    const personRepository = project.getRepository();
    const entity = await personRepository.loadEntity(PersonEntity, opts.uuid);

    if (opts.decrypt) {
      entity.setPropertyEncription(opts.param, false);
    }

    if (opts.encrypt) {
      entity.setPropertyEncription(opts.param, true);
    }

    if (opts.value !== undefined) {
      entity.set(opts.param, opts.value);
    }
    await personRepository.saveEntity(entity);
    entity.print();
  });

/* tslint:disable-next-line */
commander.parseAsync(process.argv);

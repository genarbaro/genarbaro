import * as commander from "commander";
import { GenarbaroRegistry } from "./infrastructure/genarbaro.registry";
import { RelationEntity } from "./model/relation.entity";

commander
  .command("create")
  .description("create a new relation file")
  .option("-i, --interactive") // TODO: Add support
  .action(async (opts) => {
    const project = await GenarbaroRegistry.create();

    const repository = project.getRepository();
    const entity = await repository.createEntity(RelationEntity);
    console.log(`New entity id: ${entity.id}`);
    // Save empty
    await repository.saveEntity(entity);
  });

commander
  .command("list")
  .description("list all relation")
  .action(async (opts) => {
    const project = await GenarbaroRegistry.create();

    const repository = project.getRepository();
    const uuids = await repository.list(RelationEntity);
    const entities = await Promise.all(
      uuids.map(async (uuid) => {
        return repository.loadEntity(RelationEntity, uuid);
      })
    );

    entities.forEach((entity) => {
      entity.print();
    });
  });

/* tslint:disable-next-line */
commander.parseAsync(process.argv);
